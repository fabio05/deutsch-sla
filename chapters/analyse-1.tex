\selectlanguage{ngerman}%
\chapter{Eckberts und Berthas Rückzug aus der Gesellschaft}%
\label{chap:analyse1}

\section{Inhaltliche Zusammenfassung}

Eckbert und seine Frau Bertha leben in einer Burg abseits von der Gesellschaft.
Eckberts Freund Walther ist der einzige, der regelmässig vorbeikommt. An einem
Abend mit Walther erzählt Bertha die Geschichte ihrer Jugend:

Von ihrem Vater, einem armen Hirten, wurde sie grob behandelt, da sie ungeschickt
war. Eines Morgens, mit ungefähr acht Jahren, verliess sie die Hütte und
flüchtete in die Weite. Auf ihrem Weg schwankte sie andauernd zwischen der
Sehnsucht zu leben und der zu sterben. Mehrere Tage wanderte sie schon, als
sie einer alten Frau begegnete, die sie in ihre Hütte mitnahm. Dort lebte sie
für die nächste Zeit mit der Alten, ihrem Hund und einem wunderbaren Vogel, der
täglich ein Ei legte, in dem sich eine Perle oder ein Edelstein befand. Der Vogel
sang immer ein Lied um das Schlüsselwort „Waldeinsamkeit“. Sie lernte
Spinnen und Lesen und kümmerte sich um den Haushalt, wenn die Alte auf Reisen war.
Nach und nach wuchs ihre Sehnsucht nach der Welt der schönen Ritter, von der sie
gelesen hatte, und mit vierzehn Jahren, als die Alte für eine längere Reise
fortging, entschloss sie, sich selbst auf den Weg zu machen und nahm den Vogel
und eines der Gefässe mit Edelsteinen mit. Nach mehreren Tagen kam sie in dem
Dorfe an, in dem sie geboren war, und sie suchte ihre Eltern auf, doch in ihrem
Hause sassen fremde Leute, die ihr sagten, dass ihre Eltern vor drei Jahren
gestorben seien. In einer angenehmen Stadt mietete sie sich ein Haus mit einem
Garten. Der Vogel, der lange Zeit nicht gesungen hatte, begann laut ein neues
Lied zu singen, worauf Bertha ihm den Hals umdrehte. Später lernte sie den
jungen Eckbert kennen und nahm ihn zum Mann.

Walther dankt ihr für die Geschichte und sagt, „ich kann mir euch
recht vorstellen, [...] wie ihr den kleinen \emph{Strohmian} füttert.“
\footfullcite[S. 18]{Tieck:Eckbert}
Als Bertha am nächsten Morgen erkrankt und ihre Krankheit von Tag zu Tag
schlimmer wird, lässt sie an einem Morgen Eckbert an ihr Bett rufen und erzählt
ihm, dass Walther den Namen des kleinen Hundes wusste, obwohl sie diesen nie
erwähnt hatte. Später geht Eckbert auf die Jagd, sieht Walther in der Ferne und
erschiesst ihn mit seiner Armbrust. Bei seiner Rückkehr ist Bertha tot. Eckbert
fällt in Einsamkeit und lebt unter inneren Vorwürfen. Getrieben vom Wunsch nach
inniger Freundschaft, schliesst er bald eine solche mit dem Ritter Hugo. Als
Eckbert ihn aber in seine Geschichte einweiht, verhält jener sich immer
distanzierter und Eckbert meint an ihm Walthers Gesichtszüge zu entdecken.
Entsetzt ergreift er die Flucht, kehrt auf seine Burg zurück und beschliesst
fortzureisen, meint aber immer wieder Walthers Gesicht in unterschiedlichen
Menschen zu sehen. Schliesslich trifft er im Wald die Alte, die ihn über seine
wirre Vergangenheit aufklärt. Eckbert stirbt verwirrt.

\section{Philister und Romantiker}

Laut Romantik gibt es zwei Lebenswege: Als Philister, oder als Romantiker.
Dies beschreibt auch Joseph von Eichendorff in seinem Gedicht „Die zwei
Gesellen“:

\begin{quotation}\noindent
    Die strebten nach hohen Dingen, \\
    Die wollten, trotz Lust und Schmerz, \\
    Was Rechts in der Welt vollbringen, \\
    {[\dots]} \\
    \\
    Der erste, der fand ein Liebchen, \\
    Die Schwieger kauft' Hof und Haus; \\
    Der wiegte gar bald ein Bübchen, \\
    Und sah aus heimlichem Stübchen \\
    Behaglich ins Feld hinaus. \\
    \\
    Dem zweiten sangen und logen \\
    Die tausend Stimmen im Grund, \\
    Verlockend' Sirenen, und zogen \\
    Ihn in der buhlenden Wogen \\
    Farbig klingenden Schlund.
    \footfullcite[S. 52f]{Eichendorff:2Gesellen}
\end{quotation}

Er spricht von zwei Gesellen, die ihr Zuhause verlassen und einen Neubeginn
wagen. Deren Ausgangspunkt scheint gleich zu sein. Doch der Erste, der Philister
findet eine Geliebte, gründet eine Familie und wird sesshaft. Währenddessen
endet der Zweite, der Romantiker unterwegs im Untergang, von Sirenen hinab gezogen.

Philister und Romantiker sind aber nicht zwei getrennte Lebenswege, sondern,
wie es am Taugenichts in Joseph von Eichendorffs
„Aus dem Leben eines Taugenichts“ deutlich wird, gegensätzliche
Charakteristiken und Realisierungen bei einem Menschen, die durchaus hin und her
schwanken können.
\footcite[Vgl.][S. 142]{Bark:Romantik}
Allerdings wird in der Romantik die Definition der beiden meist auseinandergehalten.

\subsection{Philister}

Philister werden als konservativer Gegner der Romantik und Romantiker betrachtet.
Sie sind Leute, die „nur ein Alltagsleben“, also nur ein Leben mit
sich immer wiederholenden Handlungen, leben. Ihr Alltag ist durchgeplant und
monoton und sie haben und nehmen sich keine Zeit um die Welt an sich zu erleben.
Das poetische grösste Ausmass, dass sie erreichen, ist in der Religion. Diese
ist für sie wie ein Beteubungsmittel, von dem sie abhängig sind und dass sie
wie Morgen- und Abendmahl in form eines Gebetes einnehmen.
\footfullcite[Vgl.][S. 18, Nr. 77]{Novalis:Blüthenstaub}

Wie es auch im Taugenichts gezeigt wird, tragen Philister typischerweise eine
Schlafmütze, einen Schlafrock und Pantoffeln und rauchen mit Pfeifen Tabak.
\footfullcite[Vgl.][S. 15]{Eichendorff:Taugenichts}

\section{Die Fluchten der Epoche}

In dieser Arbeit wird die Flucht im Sinn von Rückzug vor verschiedensten
Faktoren gesehen. Im Folgenden sind Aspekte aufgezeigt, die in der Romantik
eine entscheidende Rolle spielen:

\paragraph{Flucht aus der Zeit, in der Tieck lebt}
Wie man aus blonden Eckberts ritterlichen Leben in einer Burg schliessen kann,
wählt Tieck sein Märchen in der Zeit des Mittelalters einzubetten. Das
Mittelalter wird in der Spätromantik intensiv romantisiert und glorifiziert.
Für Tieck und viele weitere Romantiker ist das Mittelalter eine heile Zeit.

\paragraph{Flucht von der Unterdrückung durch die alte Ordnung}
Mit der Niederlage von Napoleon und dem Wiener Kongress werden die Ideale der
Französischen Revolution (Freiheit, Gleichheit, Brüderlichkeit) zunichtegemacht
und die Ordnung vor der Französischen Revolution wiederhergestellt. Somit
verlieren die Bürger ihre demokratischen Rechte und müssen sich wieder einem
Alleinherrscher unterwerfen.

\begin{figure}[H]
    \includegraphics[width=\linewidth]{waldeinsamkeit.jpg}
    \caption{\citetitle{Fig:Waldeinsamkeit}}
\end{figure}

\paragraph{Flucht von Vernunft und Rationalismus der Aufklärung}
Während in der Aufklärung der Verstand im Mittelpunkt steht und die
Schriftsteller Werke schreiben, um die Bevölkerung zu erziehen und aufzuklären,
fühlen sich die Romantiker als Aussenseiter und setzen die Gefühle, die Natur
und die Mystik ins Zentrum der Weltanschauung. Tieck sprengt den Rahmen des
Verstandes, indem er das Schaurige, das Fantastische, das Gefühlvolle und das
Abenteuerliche als zentrale Motive seines Werkes wählt.

Im Blonden Eckbert erleben die Protagonisten Bertha und Eckbert eine
fantasievolle Welt: Die mystisch-magische Gegend, der sprechende Vogel, der
Gedichte um die „Waldeinsamkeit“ in menschlicher Sprache singt und jeden Tag
ein Ei mit einer Perle oder einem Edelstein legt, sowie die alte Frau, als
Motiv des Doppelgängers (Walther, Hugo, der Bauer).

\paragraph{Flucht aus der kapitalistischen Mentalität}
In der Romantik war das kapitalistische Denken, vor allem in der Arbeitswelt
weit verbreitet und stellte im Vergleich zum romantischen Denken nicht die
Gefühle des einzelnen in den Vordergrund, sondern bewertete diesen aufgrund
seiner Arbeitseffizienz. Davon wandten sich die Romantiker ab und lehnten sich
gegen das philiströse Grossbürgertun auf, und verspotteten diese.

\section{Flucht und Reisen im blonden Eckbert}

Im Werk „Der blonde Eckbert“ treffen wir Flucht und Reisen auf unterschiedliche
Weisen an. Einerseits ist es alleinig eine Reise, oder eine Flucht, welche in
eine Reise übergeht, ohne vorbestimmte Destination, aber mit dem Ziel eines
besseren Lebens, auch genutzt, um die Natur zu romantisieren. Andererseits
kommt es als Flucht, wie in den oben genannten „Fluchten der Epoche“, im Sinn
von Rückzug vor bestimmten Faktoren, vor.

\subsection{Reise}

\paragraph{Berthas Aufbruch von Zuhause}
Weil Bertha von ihrem Vater schlecht behandelt wird und der Verzweiflung nahe
ist, flüchtet sie eines Morgens aus dem Haus ihrer Eltern. Die Angst, dass ihr
Vater sie einholen würde, treibt sie zu Beginn voran:

\begin{quotation}\noindent
    „Ich lief immerfort, ohne mich umzusehen, ich fühlte keine Müdigkeit, denn
    ich glaubte immer, mein Vater würde mich noch wieder einholen und, durch
    meine Flucht gereizt, mich noch grausamer behandeln.“
\end{quotation}

Sie durchquert Wälder, geht über Gebirge und bettelt in mehreren
Dörfern um Speis und Trank. Nach ungefähr vier Tagen Fortwanderung und einer
Nacht auf einem Felsen in der Wildnis ist sie erschöpft und müde. Sie setzte
sich „nieder und beschloss zu sterben. Aber nach einiger Zeit trug die Lust zu
leben dennoch den Sieg davon“. Sie rafft sich auf und setzt ihre Reise mit
unbeschreiblicher Sehnsucht einen Menschen zu treffen. Und dann plötzlich geht
die Flucht in eine Reise über und Bertha erlebt die Waldeinsamkeit:

\begin{quotation}\noindent
    „Gegen Abend schien die Gegend umher etwas freundlicher zu werden; meine
    Gedanken, meine Wünsche lebten wieder auf, die Lust zum Leben erwachte in
    allen meinen Adern. Ich glaubte jetzt, das Gesause einer Mühle aus der
    Ferne zu hören, ich verdoppelte meine Schritte, und wie wohl, wie leicht
    ward mir, als ich endlich wirklich die Grenzen der öden Felsen erreichte!
    Ich sah Wälder und Wiesen mit fernen angenehmen Bergen wieder vor mir
    liegen. Mir war, als wenn ich aus der Hölle in ein Paradies getreten wäre,
    die Einsamkeit und meine Hülflosigkeit schienen mir nun gar nicht
    fürchterlich.“
\end{quotation}

An dem Bach mit dem Wasserfall trifft sie die alte Frau, welche ihr Brot und
Wein gibt und sie mit in ihr Haus nimmt.
\footcite[Vgl.][S. 6f]{Tieck:Eckbert}

\paragraph{Bertha verlässt die Waldeinsamkeit}
Mit ihrem Heranreifen will Bertha die neue Welt erleben, von der sie in den
Büchern der alten Frau gelesen hat. Als die Alte auf eine längere Reise
aufbricht, verlässt Bertha das Haus, bindet den kleinen Hund fest, nimmt aber den
Käfig mit dem Vogel und ein mit Edelsteine gefülltes Gefäss mit. Sie reist eine
Weile und endet im Dorf, in dem ihre Eltern lebten, doch diese waren schon
verstorben.
\footcite[Vgl.][S. 15f]{Tieck:Eckbert}

\begin{quotation}\noindent
    „Meine Reise war ziemlich einförmig aber je weiter ich ging, je mehr
    ängstigte mich die Vorstellung von der Alten und dem kleinen Hunde; ich
    dachte daran, dass er wahrscheinlich ohne meine Hülfe verhungern müsse;
    im Walde glaubt' ich oft, die Alte würde mir plötzlich entgegentreten.
    So legte ich unter Tränen und Seufzern den Weg zurück; sooft ich ruhte
    und den Käfig auf den Boden stellte, sang der Vogel sein wunderliches
    Lied, und ich erinnerte mich dabei recht lebhaft des schönen verlassenen
    Aufenthalts. Wie die menschliche Natur vergesslich ist, so glaubt' ich
    jetzt, meine vormalige Reise in der Kindheit sei nicht so trübselig
    gewesen als meine jetzige; ich wünschte wieder in derselben Lage zu
    sein.“
\end{quotation}

\paragraph{Eckberts letzte Reise}
Eckbert vertraut Hugo seine ganze Geschichte an und wurde von ihm verraten und
missachtet. Wahnvorstellungen verfolgen Eckbert, denn er entdeckt an Hugo die
Gesichtszüge von Walther. Mit dem festen Entschluss, nie wieder einen Menschen
näher an sich heranzulassen, zieht Eckbert schliessich ohne ein bestimmtes Ziel
zu haben in die Fremde. Er verirrt sich in einem Gebirge und gelangt zum
Wasserfall, an dem Jahre zuvor Bertha der alten Frau begegnet war. Er reitet
weiter und trifft die Alte bei ihrem Haus. Anschliessend endet er in einem
Wahn und stirbt.
\footcite[Vgl.][S. 22f]{Tieck:Eckbert}

\subsection{Flucht}

\paragraph{Eckberts und Berthas Burg}
Im Kontrast zur Waldeinsamkeit, die Bertha bei der alten Frau erlebt, steht
die selbst ausgewählte Isolation in der Burg. Diese spielt eine entscheidende
Rolle, als Schutz vor der realen Welt, vor der Gesellschaft und der Erfahrungen
aus der Vergangenheit (Bertha). Ihre Einsamkeit erleben sie aber nicht nur im
Leben, sondern auch bei ihrem einsamen Tod.

Die Burg und das Leben der Ritter zeigen die romantische Idealisierung des
Mittelalters und der damit verbundene Rückzug aus der Industrialisierung.

Für romantische Protagonisten eines romantischen Werkes ist es allerdings
paradox, wie die selbstgewählte Einsamkeit von Eckbert und Bertha mehr auf eine
philiströse, gewöhnliche als auf eine romantische, glückliche Lebensart mit
Höhen und Tiefen hinweist.
\footcite[Vgl.][S. 3]{Tieck:Eckbert}

\begin{quotation}\noindent
    „Er lebte sehr ruhig für sich und war niemals in den Fehden seiner Nachbarn
    verwickelt, auch sah man ihn nur selten ausserhalb den Ringmauern seines
    kleinen Schlosses. Sein Weib liebte die Einsamkeit ebenso sehr.“
\end{quotation}

\paragraph{Eckbert nach dem Mord an Walther}
Nach dem Mord an Walther und dem Tod von Bertha zieht sich Eckbert immer mehr
zurück. Sogar eine neue Freundschaft scheitert an seinem mittlerweile gewachsenen
Verfolgungswahn. Er flüchtet aus einem Leben, das für ihn zu schrecklich geworden
ist. Der Schrecken nimmt aber kein Ende und führt zu seinem baldigen Tod -- der
ultimativen Flucht aus dem Leben.
\footcite[Vgl.][S. 21]{Tieck:Eckbert}
