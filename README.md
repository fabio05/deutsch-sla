Deutsch SLA
===========

A LaTeX document.

Authoring with LaTeX
--------------------

`.tex` documents can be edited with a simple text editor or programming IDE,
and compiled with `latexmk` and/or `pdflatex`.

### Editor Setup (Example)

1. [Codium](https://vscodium.com/) IDE
1. James Yu's [latex-workshop](https://open-vsx.org/extension/James-Yu/latex-workshop) plugin,
   plus Julian Valentin's [spell checker](https://open-vsx.org/extension/valentjn/vscode-ltex)
1. A [LaTeX distribution](https://www.latex-project.org/get/), including `latexmk`

e.g.
```console
codium --install-extension james-yu.latex-workshop
codium --install-extension valentjn.vscode-ltex
```
```console
sudo apt-get install biber texlive texlive-lang-german texlive-bibtex-extra texlive-latex-extra latexmk
```

### Working in the Terminal

Build PDF document:

```console
latexmk -pdf
```

Clean up build files:

```console
find . -name '*.aux' -delete -print && rm -v *.{bbl,bcf,blg,lo?,out,toc,fls,fdb*,gz,run.xml}
```

### References

- [LaTeX cheat sheet](https://wch.github.io/latexsheet/)
- [LaTeX tutorial](https://latex-tutorial.com/tutorials/first-document/) (English)
- [LaTeX tutorial](https://latex.tugraz.at/latex/tutorial) (German)
- [VS Code as LaTeX editor](https://danmackinlay.name/notebook/vs_code_for_latex.html)
- [BibTeX: A complete guide](https://www.bibtex.com/g/bibtex-format/)
- [BibTeX styles](https://www.reed.edu/cis/help/LaTeX/bibtexstyles.html)
